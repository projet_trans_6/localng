import { Component, OnInit, Input } from '@angular/core';
import { Response_activity } from 'app/_models/activity_model';
import { UploadService } from 'app/_services/upload.service';
import { ActivatedRoute, Router, Params} from '@angular/router';

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css']
})
export class ActivityComponent implements OnInit {

  public activity: Response_activity;
  public loading: boolean;

  constructor(private uploadService: UploadService, private router: Router, private route: ActivatedRoute) {} 
    
  onBack(){
    this.router.navigate(['/runs']);
  }

  onTreat(){
    //traitement de correction des données en entrée
  }
  // onDelete(){
  //   this.uploadService.deleteFile(this.activity.id).then(
  //     () => {
  //       this.loading = false;
  //       this.router.navigate(['/runs']);
  //     }
  //   );
  // }
  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.uploadService.getFileById(+params).subscribe(
          data => console.log(data)
        );
      }
    );
  }
  
}
