import { AuthenticationService } from './../../_services/authentication.service';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule, NgForm } from '@angular/forms';

import {HttpClientModule} from '@angular/common/http';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { AdminLayoutRoutes } from './admin-layout.routing';

import { UserComponent } from '../../user/user.component';
import { MapsComponent } from '../../maps/maps.component';
import { LoginPageComponent } from 'app/auth/login-page/login-page.component';
import { Globals } from 'app/_helpers/globals';
import { ActivityComponent } from 'app/activity/activity.component';
import { ActivitiesComponent } from 'app/activities/activities.component';
import { RegestrationPageComponent } from 'app/auth/regestration-page/regestration-page.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    LeafletModule.forRoot(),
    HttpClientModule,
  ],
  declarations: [
    UserComponent,
    ActivitiesComponent,
    MapsComponent,
    LoginPageComponent,
    RegestrationPageComponent,
    ActivityComponent
  ],
  providers: [AuthenticationService, Globals, Map]
})

export class AdminLayoutModule {}
