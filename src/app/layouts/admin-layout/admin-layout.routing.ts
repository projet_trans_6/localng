import { Routes } from '@angular/router';

import { LoginPageComponent } from './../../auth/login-page/login-page.component';
import { UserComponent } from '../../user/user.component';
import { ActivitiesComponent } from '../../activities/activities.component';
import { MapsComponent } from '../../maps/maps.component';
import { ActivityComponent } from 'app/activity/activity.component';
import { RegestrationPageComponent } from 'app/auth/regestration-page/regestration-page.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'user',           component: UserComponent },
    { path: 'runs',          component: ActivitiesComponent },
    { path: 'run/:id',          component: ActivityComponent },
    { path: 'maps',           component: MapsComponent },
    { path: 'login',          component: LoginPageComponent },
    { path: 'register',          component: RegestrationPageComponent }
];
