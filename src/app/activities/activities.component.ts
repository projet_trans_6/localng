import { UploadService } from 'app/_services/upload.service';
import { Component, OnInit } from '@angular/core';
import { Response_activity, Record } from 'app/_models/activity_model';


@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css']
})
export class ActivitiesComponent implements OnInit {
  activities: any; 

  constructor(private uploadService: UploadService) { }

  ngOnInit() {
    this.uploadService.getFile().subscribe(
      data => {
        this.activities = data;
      }
    );
  }
}
