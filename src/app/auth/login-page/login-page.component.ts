import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../_services/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  email: string
  password: string
  
  returnUrl: string;
  error = false;

  constructor(private autService: AuthenticationService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit() {
    // login out the user
    this.autService.logout();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';
    console.log(this.email)
    
  }

  onEmail (event) {
    console.log(event)
  }

  onClickSubmit(formData) {
   
    this.autService.login(formData.email, formData.password).subscribe(data => {
      console.log("loggin ok");
      this.router.navigate([this.returnUrl])
    },
    error => {
      console.log(error);
      this.error = true;
    });
 }

  // loginRequest() {
  //   console.log(this.email, this.password);
    // this.autService.login(this.email, this.password).subscribe(data => {
    //     console.log("loggin ok");
    //     this.router.navigate([this.returnUrl])
    //   },
    //   error => {
    //     console.log(error);
    //     this.error = true;
    //   });
  // }

}
