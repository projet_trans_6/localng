import { Component, OnInit } from '@angular/core';
import { RegistrationRequest } from 'app/_models/api-request-response';
import { AuthenticationService } from 'app/_services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-regestration-page',
  templateUrl: './regestration-page.component.html',
  styleUrls: ['./regestration-page.component.scss']
})
export class RegestrationPageComponent implements OnInit {
  registerModel = new RegistrationRequest();
  returnUrl: string;

  constructor(private autService: AuthenticationService, private router: Router, private route: ActivatedRoute) {
  }


  ngOnInit() {
    // login out the user
    this.autService.logout();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  registerRequest() {
    console.log(this.registerModel);
    this.autService.signUp(this.registerModel)
      .subscribe(data => {
          console.log("registration ok");
          this.router.navigate([this.returnUrl])
        },
        error => {
          console.log(error);
          //this.error = true;
        })
  }
}
