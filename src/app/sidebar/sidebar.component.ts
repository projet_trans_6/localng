import { User } from 'app/_models/user';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'app/_services/authentication.service';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    { path: '/maps', title: 'Maps',  icon:'pe-7s-map-marker', class: '' },
    { path: '/runs', title: 'Table List',  icon:'pe-7s-note2', class: '' },
];


@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  currentUser: User;
  menuItems: any[];
  UserInfo: RouteInfo = { path: '/user', title: 'User Profile',  icon:'pe-7s-user', class: '' };

  constructor(private authenticationService: AuthenticationService) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser=x);
   }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  };

  public get isAuth() {
    // return true
    if (this.currentUser==null)
      return false
    return this.currentUser && this.currentUser.token 
  }
  logout() {
    this.authenticationService.logout()
  }
}
