import {Injectable} from "@angular/core";

@Injectable()
export class Globals {
  readonly apiUrl:string = 'http://localhost:8000';
}
