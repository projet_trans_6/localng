import { MapService } from './../_services/map.service';
import { Component, OnInit } from '@angular/core';

declare let L: any;
declare let omnivore: any;
let access_token = "pk.eyJ1Ijoic3lhbGlvdTI1NCIsImEiOiJjanhmMHF3bmkwc2VxM3hzOHY5ZDk1YTc3In0.lxYD7PoeBQl0GG0bxkmbAQ"
@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})

export class MapsComponent implements OnInit {
  options: any
  L:any
  DJANGO_SERVER = 'http://127.0.0.1:8000'
  geojsonURL: string
  jsonData: string

  
  constructor(private _mapService: MapService) { }

  ngOnInit() {
    
    // this.options = {
    //   layers: [
    //     L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
    //   ],
    //   zoom: 13,
    //   center: L.latLng(46.879966, -121.726909)
  
    // };
    var mymap = L.map('map').setView([14.7168, -17.3704], 12);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
      .addTo(mymap);

    // L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    // attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    // maxZoom: 18,
    // id: 'mapbox.streets',
    // accessToken: access_token
    // }).addTo(mymap);

    // var runLayer = omnivore.gpx('/mapbox.js/assets/data/run.gpx')
    // .on('ready', function() {
    //     map.fitBounds(runLayer.getBounds());
    // })
    // .addTo(map);

    this._mapService.file$
      .subscribe(
        file => {console.log("file receaved")
        console.log(file)
        this.geojsonURL = `${this.DJANGO_SERVER}${file.file_processed}`
        let self = this
        fetch(this.geojsonURL)
          .then(function(response) {
            return response.json();
          })
          .then(function(myJson) {
            console.log(JSON.stringify(myJson));
            self.jsonData = JSON.parse(JSON.stringify(myJson))
            var myStyle = {
              "color": "#000000",
              "weight": 2,
              "opacity": 0.65
              };
              // var mymap = L.map('map').setView([14.7168, -17.3704], 12);

              L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
                .addTo(mymap);

            console.log(Object.keys(self.jsonData))
             L.geoJSON(self.jsonData, {
               style: myStyle
            }).addTo(mymap);
           });
        })
}

}
