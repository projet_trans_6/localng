import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Response_activity } from 'app/_models/activity_model';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  
  private _fileSource = new Subject<Response_activity>();
  file$ = this._fileSource.asObservable();
  constructor() { }

  sendFile(file: Response_activity) {
    this._fileSource.next(file);
  }

}
