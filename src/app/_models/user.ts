export class User {
  id: bigint;
  email: string;
  name: string;
  user_type: string;
  token: string;
}
