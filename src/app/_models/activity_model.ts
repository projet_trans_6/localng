import { User } from 'app/_models/user';

export class Response_activity {
    id : bigint
    file: string
    file_processed: string
}

export class Record {
    name: string
    date: Date
    description: string
    file: Response_activity
    user_id: number
}