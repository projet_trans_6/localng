export class RegistrationRequest {
  email: string = '';
  password1: string = '';
  password2: string = '';
  user_type: string = '';
}
