import { MapService } from './../../_services/map.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from '../../sidebar/sidebar.component';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { User } from 'app/_models/user';
import { AuthenticationService } from 'app/_services/authentication.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UploadService } from 'app/_services/upload.service';

@Component({
    // moduleId: module.id,
    selector: 'navbar-cmp',
    templateUrl: 'navbar.component.html'
})

export class NavbarComponent implements OnInit{
    currentUser: User;
    private listTitles: any[];
    location: Location;
    private toggleButton: any;
    private sidebarVisible: boolean;

    DJANGO_SERVER = 'http://127.0.0.1:8000'
    form: FormGroup;
    response;
    gpxURL;


    constructor(private formBuilder: FormBuilder,
                location: Location,  
                private element: ElementRef, 
                private authenticationService: AuthenticationService, 
                private uploadService: UploadService,
                private _mapService: MapService) {
        this.location = location;
        this.sidebarVisible = false;
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);

    }

    ngOnInit(){
      this.listTitles = ROUTES.filter(listTitle => listTitle);
      const navbar: HTMLElement = this.element.nativeElement;
      this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];

      this.form = this.formBuilder.group({
        profile: ['']
      });

    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function(){
            toggleButton.classList.add('toggled');
        }, 500);
        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };

    getTitle(){
      var titlee = this.location.prepareExternalUrl(this.location.path());
      if(titlee.charAt(0) === '#'){
          titlee = titlee.slice( 1 );
      }

      for(var item = 0; item < this.listTitles.length; item++){
          if(this.listTitles[item].path === titlee){
              return this.listTitles[item].title;
          }
      }
      return 'Map';
    }

    
    onChange(event) {
      if (event.target.files.length > 0) {
        const file = event.target.files[0];
        this.form.get('profile').setValue(file);
      }
    }
    
    onSubmit() {
        const formData = new FormData();
        formData.append('file', this.form.get('profile').value);
        
        this.uploadService.upload(formData).subscribe(
          (res) => {
            this.response = res;
            this.gpxURL = `${this.DJANGO_SERVER}${res.file}`;
            console.log(res);
            console.log(this.gpxURL);
            this._mapService.sendFile(res)
          },
          (err) => {  
            console.log(err);
          }
          );
        }
        
    public get isAuth() {
      if (this.currentUser == null)
        return false
        return this.currentUser && this.currentUser.token
      }
        logout() {
          this.authenticationService.logout()
      }

}
