import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './navbar.component';
import { User } from 'app/_models/user';
import { AuthenticationService } from 'app/_services/authentication.service';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [ RouterModule, CommonModule,ReactiveFormsModule,
      HttpClientModule ],
    declarations: [ NavbarComponent ],
    exports: [ NavbarComponent ]
})

export class NavbarModule implements OnInit {
    currentUser: User;
  
    constructor(private authenticationService: AuthenticationService) {
      this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  
    }
    public get isAuth(){
      return this.currentUser && this.currentUser.token
    }
    ngOnInit() {
    }
    logout() {
      this.authenticationService.logout()
    }
}
